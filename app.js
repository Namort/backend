var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var MongoClient = require("mongodb").MongoClient;
const mongoose = require('mongoose');
const cors = require("cors");
const config = require('./config/database.config');

const indexRouter = require('./routes/index');

const artistRouter = require('./routes/artists');
const albumRouter = require('./routes/Album');
const trackRouter = require('./routes/track');


// on se connecte � la base de donn�es
mongoose.connect(config.url, { useNewUrlParser: true });
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/artists', artistRouter);
app.use('/album', albumRouter);
app.use('/track', trackRouter);




module.exports = app;
