const Artist = require('../models/artist.model.js');
var express = require('express');
var router = express.Router();

/* Afficher tous les artistes. */
router.get('/',(req,res) => {
  Artist.find()
  .then(function(artists){
  	  res.send(artists);
  })
  .catch(err=>{
  	  res.status(500).send({
	  	  message: err.message || 'Il y a eu des erreur lors de la recherche d artistes.'
	  });
  });
});

/*trouver un artist par ID*/
router.get('/:id',(req,res) => {
  Artist.findById(req.params.id)
    .then(artist => {
      if (!artist) {
        return res.status(404).send({
          message: 'Artiste pas trouve avec l id ' + req.params.id
        });
      }
      res.send(artist);
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Artiste pas trouve avec l id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Une erreur est survenu dans la r�cup�ration d un artiste' + req.params.id
      });
    });
});

/*faire un nouvel Artist*/
router.put('/', (req, res) => {
  // valider la requ�te
  if (!req.body.Nom) {

    return res.status(400).send({
      message: 'le nom ne peut pas etre vide'
    });
  }
   if (!req.body.Followers) {

    return res.status(400).send({
      message: 'les Followers ne peut pas etre vide'
    });
  }
   if (!req.body.Birth) {

    return res.status(400).send({
      message: 'la date de naissance ne peut pas etre vide'
    });
  }
 // Cr�er un nouvel artiste
  const artist = new Artist({
    Nom: req.body.Nom,
    Birth: req.body.Birth,
	Followers: req.body.Followers,
	Album: req.body.Album 
  });
   // sauvegarder l'artiste
  artist.save().then(data => {
      
      res.send(data);
    })
    .catch(err => {
      
      res.status(500).send({
        message: err.message || 'Une erreur est survenu dans la sauvegarde de l artiste.'
      });
    });
});


/* Supprimer un artiste */
router.delete('/:id', (req, res) => {
  Artist.findByIdAndRemove(req.params.id)
    .then(artist => {
      if (!artist) {
        return res.status(404).send({
          message: 'Artiste pas trouve avec l id : ' + req.params.id
        });
      }
      res.send({ message: 'Artiste a bien ete Supprime' });
    })
    .catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: 'Artiste pas trouve avec l id :' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Artice n a pas pu ete Supprime son id ete le ' + req.params.id
      });
    });
});

/* Modifier un artiste */ 

router.post('/:id', (req, res) => {

  // Validation
  if (!req.body.Nom) {
    return res.status(400).send({     
	message: 'le nom ne peut pas etre vide'
    });
  }
   if (!req.body.Followers) {

    return res.status(400).send({
      message: 'les Followers ne peut pas etre vide'
    });
  }
   if (!req.body.Birth) {

    return res.status(400).send({
      message: 'la date de naissance ne peut pas etre vide'
    });
  }
  // Trouver un artiste et le modifier avec le nouveau body
  Artist.findByIdAndUpdate(
    req.params.id,
    {
    Nom: req.body.Nom,
    Birth: req.body.Birth,
	Followers: req.body.Followers,
	Album: req.body.Album || ''
    },
    { new: true }
  )
    .then(artist => {
      if (!artist) {
        return res.status(404).send({
          message: 'Artiste pas trouve avec l id :  ' + req.params.id
        });
      }
      res.send(artist);
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Artiste pas trouve avec l id : ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Artice n a pas pu ete mis a jour son id ete le' + req.params.id
      });
    });
});


module.exports = router;
