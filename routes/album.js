const Album = require('../models/album.model.js');
var express = require('express');
var router = express.Router();

/* Afficher tous les albums. */
router.get('/',(req,res) => {
  Album.find()
  .then(function(albums){
  	  res.send(albums);
  })
  .catch(err=>{
  	  res.status(500).send({
	  	  message: err.message || 'Il y a eu des erreur lors de la recherche d albums.'
	  });
  });
});


/* r�cup�rer un genre */ 
router.get('/genre', (req,res)=>{
	Album.aggregate([
		{$group : {
			_id : "$Genre",
			nbm : { $sum :1} } }
	])
	.then(function(album){
  	  res.send(album);
  })
  .catch(err=>{
  	  res.status(500).send({
	  	  message: err.message || 'Il y a eu des erreur lors de la recherche d un album.'
	  });
  });
});

/* Afficher  un titre al�atoire. */
router.get('/alea',(req,res) => {
  Album.aggregate([
	{ $sample: { size: 1 }}
  ])
  .then(function(album){
  	  res.send(album);
  })
  .catch(err=>{
  	  res.status(500).send({
	  	  message: err.message || 'Il y a eu des erreur lors de la recherche d un album.'
	  });
  });
});


/*trouver un album par ID*/
router.get('/:id',(req,res) => {
  Album.findById(req.params.id)
    .then(album => {
      if (!album) {
        return res.status(404).send({
          message: 'Album pas trouve avec l id ' + req.params.id
        });
      }
      res.send(album);
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Album pas trouve avec l id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Une erreur est survenu dans la r�cup�ration d un album' + req.params.id
      });
    });
});


/*faire un nouvel Album*/
router.put('/', (req, res) => {
  // valider la requ�te
  if (!req.body.Title) {

    return res.status(400).send({
      message: 'le nom ne peut pas etre vide'
    });
  }
   if (!req.body.Genre) {

    return res.status(400).send({
      message: 'le genre ne peut pas etre vide'
    });
  }
  
 // Cr�er un nouvel album
  const album = new Album({
    Title: req.body.Title,
    Genre: req.body.Genre,
	Relese: req.body.Relese || '',
	Cover_url: req.body.Cover_url || '',//mettre une url par d�faut
	Tracks: req.body.Tracks
  });
   // sauvegarder l'album
  album.save().then(data => {
      
      res.send(data);
    })
    .catch(err => {
      
      res.status(500).send({
        message: err.message || 'Une erreur est survenu dans la sauvegarde de l album.'
      });
    });
});


/* Supprimer un album */
router.delete('/:id', (req, res) => {
  Album.findByIdAndRemove(req.params.id)
    .then(album => {
      if (!album) {
        return res.status(404).send({
          message: 'Album pas trouve avec l id : ' + req.params.id
        });
      }
      res.send({ message: 'Album a bien ete Supprime' });
    })
    .catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: 'Album pas trouve avec l id :' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Album n a pas pu ete Supprime son id ete le ' + req.params.id
      });
    });
});

/* Modifier un Album */ 

router.post('/:id', (req, res) => {

  // Validation
  if (!req.body.Title) {
    return res.status(400).send({     
	message: 'le nom ne peut pas etre vide'
    });
  }
   if (!req.body.Genre) {

    return res.status(400).send({
      message: 'les Followers ne peut pas etre vide'
    });
  }
  
  // Trouver un album et le modifier avec le nouveau body
  Album.findByIdAndUpdate(
    req.params.id,
    {
    Title: req.body.Title,
    Genre: req.body.Genre,
	Relese: req.body.Relese || '',
	Cover_url: req.body.Cover_url || '',//mettre une url par d�faut
	Tracks: req.body.Tracks
    },
    { new: true }
  )
    .then(album => {
      if (!album) {
        return res.status(404).send({
          message: 'Album pas trouve avec l id :  ' + req.params.id
        });
      }
      res.send(album);
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Album pas trouve avec l id : ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Album n a pas pu ete mis a jour son id ete le' + req.params.id
      });
    });
});

module.exports = router;
