const Track = require('../models/track.model.js');
var express = require('express');
var router = express.Router();

/* Afficher tous les Tracks. */
router.get('/',(req,res) => {
  Track.find()
  .then(function(tracks){
  	  res.send(tracks);
  })
  .catch(err=>{
  	  res.status(500).send({
	  	  message: err.message || 'Il y a eu des erreur lors de la recherche de tracks.'
	  });
  });
});

/* Afficher  un titre al�atoire. */
router.get('/alea',(req,res) => {
  Track.aggregate([
	{ $sample: { size: 1 }}
  ])
  .then(function(tracks){
  	  res.send(tracks);
  })
  .catch(err=>{
  	  res.status(500).send({
	  	  message: err.message || 'Il y a eu des erreur lors de la recherche d un track.'
	  });
  });
});



/*trouver une track par ID*/
router.get('/:id',(req,res) => {
  Track.findById(req.params.id)
    .then(track => {
      if (!track) {
        return res.status(404).send({
          message: 'Track pas trouve avec l id ' + req.params.id
        });
      }
      res.send(track);
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'track pas trouve avec l id ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Une erreur est survenu dans la r�cup�ration d un track' + req.params.id
      });
    });
});


/*faire un nouveau Track*/
router.put('/', (req, res) => {
  // valider la requ�te
  if (!req.body.Title) {

    return res.status(400).send({
      message: 'le titre ne peut pas etre vide'
    });
  }
 // Cr�er un nouvel track
  const track = new Track({
    Title: req.body.Title,
    Duration: req.body.Duration || '',
	Listenings: req.body.Listenings || '',
	Likes: req.body.Likes || '',
	Featuring: req.body.Featuring
  });
   // sauvegarder le track
  track.save().then(data => {
      
      res.send(data);
    })
    .catch(err => {
      
      res.status(500).send({
        message: err.message || 'Une erreur est survenu dans la sauvegarde du track.'
      });
    });
});


/* Supprimer un track */
router.delete('/:id', (req, res) => {
  Track.findByIdAndRemove(req.params.id)
    .then(track => {
      if (!track) {
        return res.status(404).send({
          message: 'Track pas trouve avec l id : ' + req.params.id
        });
      }
      res.send({ message: 'Track a bien ete Supprime' });
    })
    .catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: 'Track pas trouve avec l id :' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Track n a pas pu ete Supprime son id ete le ' + req.params.id
      });
    });
});


/* Modifier un track */ 
router.post('/:id', (req, res) => {
  // Validation
  if (!req.body.Title) {
    return res.status(400).send({     
	message: 'le tire ne peut pas etre vide'
    });
  }
  // Trouver un artiste et le modifier avec le nouveau body
  Track.findByIdAndUpdate(
    req.params.id,
    {
    Title: req.body.Title,
    Duration: req.body.Duration,
	Listenings: req.body.Listenings,
	Likes: req.body.Likes,
	Featuring: req.body.Featuring
    },
    { new: true }
  )
    .then(track => {
      if (!track) {
        return res.status(404).send({
          message: 'Track pas trouve avec l id :  ' + req.params.id
        });
      }
      res.send(track);
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: 'Track pas trouve avec l id : ' + req.params.id
        });
      }
      return res.status(500).send({
        message: 'Track n a pas pu ete mis a jour son id ete le' + req.params.id
      });
    });
});


module.exports = router;
