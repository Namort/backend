const mongoose = require('mongoose');

const trackSchema = new mongoose.Schema(
  {
    Title: {
      type: String,
      required: true
    },
    Duration: {
	type: Number
	
  },
	Listenings:{
		type : Number
		
	},
	Likes : {
		type : Number
		
	},
	Featuring : {
		type : mongoose.Schema.Types.ObjectId
	},
	},
  {
    timestamps: true
  }
  
);

module.exports = mongoose.model('Track', trackSchema,'Track');