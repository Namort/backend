const mongoose = require('mongoose');

const artistSchema = new mongoose.Schema(
  {
     Nom: {
      type: String,
     required: true
    },
    Birth: {
	type: Date,
	required: true
  },
	Followers:{
		type : Number,
		required: true
	},
	Album: {
	type :mongoose.Schema.Types.ObjectId
	},
	},
  {
    timestamps: true
  }
);

module.exports = mongoose.model('Artist', artistSchema,'Artist');