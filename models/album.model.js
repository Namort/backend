const mongoose = require('mongoose');

const albumSchema = new mongoose.Schema(
  {
     Title: {
      type: String,
      required: true
    },
    Relese: {
	type: Date
  },
	Genre:{
		type : String
	},
	Cover_url : {
		type : String
	},
	Tracks : {
		type : mongoose.Schema.Types.ObjectId
	},
	},
  {
    timestamps: true
  }
);

module.exports = mongoose.model('Album', albumSchema, 'Album');